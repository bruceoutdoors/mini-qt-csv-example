/*
Copyright (c) 2013, Ronie P. Martinez <ronmarti18@gmail.com>
Modified and hacked to take advantage of QxtCsvModel in 2013, by Lee Zhen Yong <bruceoutdoors@gmail.com>
All rights reserved.

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted, provided that the above copyright notice
and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT,
OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA
OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/

#include "miniCsvProgram.h"
#include "ui_miniCsvProgram.h"

miniCsvProgram::miniCsvProgram(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::miniCsvProgram)
{
    ui->setupUi(this);
}

miniCsvProgram::~miniCsvProgram()
{
    delete ui;
}

void miniCsvProgram::on_action_Open_triggered()
{
    anothermodel = new QxtCsvModel(this);
    fileName = QFileDialog::getOpenFileName (this, "Open CSV file",
                                                     QDir::currentPath(), "CSV (*.csv)");
    anothermodel->setSource(fileName);
    ui->tableView->setModel(anothermodel);
}

void miniCsvProgram::on_actionSave_triggered()
{
    anothermodel->toCSV(fileName);
    QMessageBox haveSaved;
    haveSaved.setText("Your have saved!");
    haveSaved.exec();
}
