/*
Copyright (c) 2013, Ronie P. Martinez <ronmarti18@gmail.com>
Modified and hacked to take advantage of QxtCsvModel in 2013, by Lee Zhen Yong <bruceoutdoors@gmail.com>
All rights reserved.

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted, provided that the above copyright notice
and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT,
OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA
OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/

#ifndef CSVREADER_H
#define CSVREADER_H

#include <QMainWindow>
#include <QMessageBox>
#include <QFileDialog>
#include <QDir>
#include <QString>
#include <QxtCsvModel>

namespace Ui {
class miniCsvProgram;
}

class miniCsvProgram : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit miniCsvProgram(QWidget *parent = 0);
    ~miniCsvProgram();
    
private slots:
    void on_action_Open_triggered();
    void on_actionSave_triggered();

private:
    Ui::miniCsvProgram *ui;
    QxtCsvModel *anothermodel;
    QString fileName;

};

#endif // CSVREADER_H
