#-------------------------------------------------
#
# Project created by QtCreator 2013-02-11T12:28:44
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = miniCsvProgram
TEMPLATE = app


SOURCES += main.cpp \
    miniCsvProgram.cpp

HEADERS  += \
    miniCsvProgram.h

FORMS    += \
    miniCsvProgram.ui

win32:CONFIG(release, debug|release): LIBS += -LC:/Qt/libqxt-Qt5/lib/ -lqxtcore
else:win32:CONFIG(debug, debug|release): LIBS += -LC:/Qt/libqxt-Qt5/lib/ -lqxtcore

INCLUDEPATH += C:/Qt/libqxt-Qt5/include/QxtCore
DEPENDPATH += C:/Qt/libqxt-Qt5/include/QxtCore


